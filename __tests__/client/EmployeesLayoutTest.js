import React from 'react'
import { shallow } from 'enzyme'
import EmployeesLayout from '../../src/client/components/employees/EmployeesLayout'
import Typography from '@material-ui/core/Typography'

describe("ShowLayout", () => {
  let useEffect;
  let wrapper;
  const mockUseEffect = () => {
    useEffect.mockImplementationOnce(f => f());
  }
  let props;
  beforeEach(() => {
    /* mocking useEffect */
    useEffect = jest.spyOn(React, "useEffect")
    mockUseEffect(); // 2 times
    mockUseEffect(); //
    /* shallow rendering */
    props = {
      fetchEmployees:jest.fn()
    }
    const myMock = jest.fn();
    wrapper = shallow(<EmployeesLayout {...props}/>);
  });
  describe("on start", () => {
    it("loads the shows", () => {
      expect(props.fetchEmployees).toHaveBeenCalled();
    });
  });
  describe("ShowsLayout", () => {
    it("should render my component", () => {
      const wrapper = shallow(<EmployeesLayout  {...props}/>)
      expect(wrapper.find(Typography).length).toEqual(1)
    })
  })
})